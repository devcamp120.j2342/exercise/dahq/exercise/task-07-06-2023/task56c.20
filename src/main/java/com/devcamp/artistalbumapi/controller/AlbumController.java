package com.devcamp.artistalbumapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbumapi.model.Album;
import com.devcamp.artistalbumapi.service.AlbumService;

@RestController
@RequestMapping("/")
@CrossOrigin

public class AlbumController {
    @Autowired
    private AlbumService albumService;

    @GetMapping("/album-info")
    public Album finAlbum(@RequestParam(required = true, name = "id") int id) {
        Album album = albumService.finAlbumById(id);
        return album;
    }

}
